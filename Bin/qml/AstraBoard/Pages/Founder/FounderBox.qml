import QtQuick 2.0
import AstraMuseum 1.0
import "../../Components/Utils/Constants.js" as Constants

Item {
    id: founderBox

    property ListModel listModel: currentListModel
    property var title: currentTitle
    property string locale : currentLocale

    Founder{
        id: box
        anchors.fill: parent
        listModel: founderBox.listModel
        title: founderBox.title
    }

    Connections{
        target: container
        onReadDone: {
            var people = (listModel.get(0))[box.locale]
            var data = people.data
            box.currentPeople.clear()
            for(var i = 0; i < data.length; i++){
                console.log(JSON.stringify(data[i]))
                box.currentPeople.append(data[i])
            }

            loadingItem.hide();
        }
    }

    Rectangle{
        width: 100
        height: 100
        radius: width/2
        antialiasing: true
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.margins: 50
        opacity: 0.5



        color: "red"

        visible: container.debugMode && !loadingItem.loading

        Text{
            text : "Back"
            anchors.centerIn: parent
        }

        MouseArea{
            anchors.fill: parent
            onClicked: {
                listModel.clear()
                loader.source = Qt.resolvedUrl("../../Settings.qml")
            }
        }
    }

    Timer{
        id: delay
        interval: 250
        onTriggered: {
            Content.init("C:/App/Data", "Founder")
            Content.list()
        }
    }

    Component.onCompleted: {
        loadingItem.show();
        delay.start()
    }
}
