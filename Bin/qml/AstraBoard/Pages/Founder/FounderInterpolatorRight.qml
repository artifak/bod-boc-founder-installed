import QtQuick 2.0
import "../../Components/Utils"

PathInterpolator {
    id: pathAnim

    property Item item
    property int ellipseWidth
    property int ellipseHeight
    property int margin
    property int duration: 10000

    function pause(){
        animation.pause()
        animationSelected.start();
    }

    path: EllipsePath{
        width: pathAnim.ellipseWidth
        height: pathAnim.ellipseHeight
        margin: 300
    }

    SequentialAnimation on progress{
        id: animationSelected
        running: false
        loops: 1

        ParallelAnimation{
            NumberAnimation {
                to: 0.85
                duration: 250
                easing.type: Easing.Linear
            }

            NumberAnimation { target: item; property: "scale"; to: 1.0; duration: 250; easing.type: Easing.Linear }
        }
    }

    SequentialAnimation on progress {
        id: animation
        running: true
        loops: Animation.Infinite

        ParallelAnimation{
            NumberAnimation {
                from: 0.675; to: 0.85
                duration: pathAnim.duration
                easing.type: Easing.Linear
            }

            NumberAnimation { target: item; property: "scale"; from: 0.8; to: 1.0; duration: pathAnim.duration; easing.type: Easing.Linear }
        }

        NumberAnimation{
            to: 0.875
            duration: pathAnim.duration/3
            easing.type: Easing.OutSine
        }

        ParallelAnimation{
            NumberAnimation {
                from: 0.875; to: 0.7
                duration: pathAnim.duration
                easing.type: Easing.Linear
            }
            NumberAnimation { target: item; property: "scale"; from: 1.0; to: 0.8; duration: pathAnim.duration; easing.type: Easing.Linear }
        }

        NumberAnimation{
            to: 0.675
            duration: pathAnim.duration/3
            easing.type: Easing.OutSine
        }
    }
}

