import QtQuick 2.0
import "../../Components/TitleText"
import "../../Components/Utils/Constants.js" as Constants

Item{
    id: boxMenu
    anchors.left: parent.left
    anchors.right: parent.right
    anchors.bottom: parent.bottom
    anchors.bottomMargin: 8

    property int selectedIndex : 0
    property alias listModel: boxMenuList.model
    property string locale: "id_ID"

    property url selectedCircleImage : "../../Assets/Images/image-circle-year-blue.png"
    property url circleImage : "../../Assets/Images/image-circle-year.png"

    property bool busy : false

    function handleClicked(){
        if(!boxMenu.busy){
            boxMenu.state = boxMenu.state == "open" ? "closed" : "open"
            if(boxMenu.state == "closed") {
                setup()
            }
        }
    }

    signal open()
    signal closed()

    property bool isOpen: false

    function hide(){
        opacity = 0.0
    }
    function show(){
        opacity = 1.0
    }

    visible: opacity > 0

    Behavior on opacity{
        NumberAnimation { duration: 500; easing.type: Easing.InOutSine }
    }

    height: 0
    onSelectedIndexChanged: {
        if(boxMenu.state == "closed"){
            boxMenuList.positionViewAtIndex(boxMenu.selectedIndex, ListView.End)
            delay.start()
        }
    }

    Image {
        source: "../../Assets/Images/image-menu.png"
        anchors.bottom: parent.top
        anchors.bottomMargin: -22
        anchors.horizontalCenter: parent.horizontalCenter

        TitleText{
            text: currentBoard.menuTitle[boxMenu.locale].toUpperCase()
            fontColor: Constants.THEME_COLOR
            fontPixelSize: 24
            shadowVisible: false
            anchors.centerIn: parent
        }

        MouseArea{
            id: mouseArea
            anchors.fill: parent
            onClicked: {
                if(!boxMenu.busy){
                    boxMenu.state = boxMenu.state == "open" ? "closed" : "open"
                    if(boxMenu.state == "closed") {
                        setup()
                    }
                }
            }
        }
    }

    Item{
        anchors.fill: parent
        anchors.topMargin: 20
        anchors.leftMargin: 180
        anchors.rightMargin: 180
        visible: boxMenu.height > 0

        ListView{
            id: boxMenuList
            anchors.fill: parent
            clip: boxMenuList.visible
            pressDelay: 2000
            cacheBuffer: 12 * 133

            delegate: Item{
                id: rootItem
                width: 133
                height: width
                //opacity: mouseArea.pressed ? 0.5 : 1.0

                property double offset: model.offset

                Behavior on offset{
                    NumberAnimation { duration: model.duration; easing.type: Easing.OutSine  }
                }

                Image{
                    anchors.centerIn: parent
                    anchors.verticalCenterOffset: rootItem.offset;

                    source: boxMenu.selectedIndex == index ? boxMenu.selectedCircleImage : boxMenu.circleImage
                    width: 113
                    height: width

                    Loader{
                        anchors.fill: parent
                        sourceComponent: yearOnly
                        asynchronous: true
                    }

                    Loader{
                        anchors.fill: parent
                        active: model.hasMonth
                        sourceComponent: monthOnly
                        asynchronous: true
                    }

                    Component{
                        id: yearOnly
                        Item{
                            width: 113
                            height: 113

                            Text{
                                id: yearText
                                anchors.centerIn: parent
                                text : model.year[boxMenu.locale]
                                font.pixelSize: model.hasMonth ?  24 : 30
                                font.family: Constants.FONT_ROMAN_FAMILY
                                anchors.verticalCenterOffset: model.hasMonth? -10 : 0
                                color: boxMenu.selectedIndex == index ? "white" : Constants.THEME_COLOR
                            }
                        }
                    }

                    Component{
                        id: monthOnly
                        Item{
                            width: 113
                            height: 113
                            Text{
                                id: monthText
                                anchors.centerIn: parent
                                anchors.verticalCenterOffset: 14
                                text: model.month[boxMenu.locale]
                                font.pixelSize: 18
                                font.family: Constants.FONT_ROMAN_FAMILY
                                color: boxMenu.selectedIndex == index ? "white" : Constants.THEME_COLOR
                            }
                        }

                    }

                    MouseArea{
                        anchors.fill: parent
                        onClicked:{
                            boxMenu.selectedIndex = index
                            delaySelection.start()
                        }
                    }
                }
            }
            orientation: ListView.Horizontal
        }
    }

    state: "closed"
    states : [
        State {
            name: "open"
        },
        State {
            name: "closed"
        }

    ]
    transitions : [
        Transition {
            from: "closed"
            to: "open"
            SequentialAnimation{
                PropertyAction{ target: boxMenu; property: "busy"; value: "true"}
                NumberAnimation { target: boxMenu; property: "height"; duration: 250; to: 150; easing.type: Easing.OutSine}
                PropertyAction{ target: delay; property: "running"; value: "true"}
            }
        },
        Transition {
            from: "open"
            to: "closed"
            SequentialAnimation{
                PropertyAction{ target: boxMenu; property: "busy"; value: "true"}
                NumberAnimation { target: boxMenu; property: "height"; duration: 500; to: 0; easing.type: Easing.OutSine}
                PauseAnimation { duration: 500 }
                PropertyAction{ target: delay; property: "running"; value: "true"}
            }
        }
    ]

    signal closedWithIndex(int idx)

    Timer{
        id: delay
        repeat: false
        interval: 100
        onTriggered: {
            if(boxMenu.state == "open"){
                for(var i = 0; i < boxMenu.listModel.count; i++){
                    boxMenu.listModel.get(i).offset = 0
                }
                boxMenu.open()
                boxMenu.isOpen = true
                //debugItem.setText('boxMenu.isOpen: ' + boxMenu.isOpen)
            }else{
                // setup menu
                boxMenu.setup()
                boxMenu.closed()
                boxMenu.isOpen = false
                boxMenu.closedWithIndex(boxMenu.selectedIndex)
                //debugItem.setText('boxMenu.isOpen: ' + boxMenu.isOpen)
            }

            boxMenu.busy = false
        }
    }

    Timer{
        id: delaySelection
        interval: 500
        onTriggered: {
            handleClicked()
        }
    }

    function setup(){

        boxMenuList.positionViewAtIndex(boxMenu.selectedIndex, ListView.End)

        var duration = 500
        var endIndex = boxMenu.selectedIndex - 12 < 0 ? 0 :  boxMenu.selectedIndex - 12

        //console.log('setup: ' + endIndex + " - " + boxMenu.selectedIndex)

        if(boxMenu.selectedIndex >= 12){
        for(var i = boxMenu.selectedIndex ; i >= endIndex; i--){
            boxMenu.listModel.get(i).offset = 300
            boxMenu.listModel.get(i).duration = duration
            duration += 100
        }}else{

            for(var j = 12; j >= 0; j--){
                boxMenu.listModel.get(j).offset = 400
                boxMenu.listModel.get(j).duration = duration
                duration += 100
            }
        }
    }

    Connections{
        target: loadingItem
        onHidden: {
            boxMenu.selectedIndex = listModel.count - 1
        }
    }
}
