import QtQuick 2.0
import "../../Components/Utils/Constants.js" as Constants

Item{

    width : 230 * Constants.GLOBAL_SCALE
    height : 100 * Constants.GLOBAL_SCALE
    property bool isID : true
    property bool active : true

    function hide(){
        opacity = 0.0
    }
    function show(){
        opacity = 1.0
    }

    function simulateSwitchLocale(){
        //debugItem.setText("called: " + active)
        if(visible) isID = !isID
    }

    Behavior on opacity {
        NumberAnimation {  duration: 500; easing.type: Easing.InOutSine }
    }

    visible: opacity > 0

    Row{
        anchors.fill: parent
        Rectangle{
            id: rect
            width : parent.height
            height : parent.height
            color: isID ? "white" : "transparent"

            radius : width/2
            border.width : 2 * Constants.GLOBAL_SCALE
            border.color: "white"

            Text {
                anchors.centerIn : parent
                text : "ID"
                font.family: Constants.FONT_LIGHT_FAMILY
                color : isID ? Constants.THEME_COLOR : "white"
                font.pointSize: 30 * Constants.GLOBAL_SCALE
            }

            antialiasing: true

            Behavior on color{
                ColorAnimation { duration: 100;}
            }

            MouseArea{
                anchors.fill : parent
                onClicked : {
                    isID = true
                }
            }
        }

        Rectangle{
            width : 30 * Constants.GLOBAL_SCALE
            height : 2 * Constants.GLOBAL_SCALE
            anchors.verticalCenter : rect.verticalCenter
        }

        Rectangle{
            width : parent.height
            height : parent.height
            color: isID ? "transparent" : "white"

            radius : width/2
            border.width : 2 * Constants.GLOBAL_SCALE
            border.color: "white"

            antialiasing: true

            Text {
                anchors.centerIn : parent
                text : "EN"
                font.family: Constants.FONT_LIGHT_FAMILY
                color : isID ? "white" : Constants.THEME_COLOR
                font.pointSize: 30 * Constants.GLOBAL_SCALE
            }

            Behavior on color{
                ColorAnimation { duration: 100;}
            }

            MouseArea{
                anchors.fill : parent
                onClicked : {
                    isID = false
                }
            }
        }
    }
}
