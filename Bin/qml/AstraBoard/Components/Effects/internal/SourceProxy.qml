/****************************************************************************
**
** Copyright (C) 2013 Labtek
** All rights reserved.
** Author: Dhi Aurrahman <dio@labtekindie.com>
**
** This file is part of the Museum Astra Project
**
****************************************************************************/

import QtQuick 2.0

Item {
    id: rootItem
    property variant input
    property variant inputItem
    visible: false

    Component.onCompleted: evaluateInput()

    function evaluateInput(){
        inputItem = input
        inputItem.visible = true
    }
}
