var fs = require('fs.extra')
var _ = require('lodash')

exports.edit = function(board, en, id, cb){
  var rootPath = __dirname + '/../../../Contents/' + board + '/People'

  var str_id_ID = fs.readFileSync(rootPath + '/id_ID.json')
  var str_en_US = fs.readFileSync(rootPath + '/en_US.json')

  var obj_id_ID = JSON.parse(str_id_ID)
  var obj_en_US = JSON.parse(str_en_US)

  if(obj_id_ID.title != id) {
    obj_id_ID.title = id 
    fs.writeFileSync(rootPath + '/id_ID.json', JSON.stringify(obj_id_ID, undefined, 2))
  }

  if(obj_en_US.title != id) {
    obj_en_US.title = en
    fs.writeFileSync(rootPath + '/en_US.json', JSON.stringify(obj_en_US, undefined, 2)) 
  }

  cb(null, { title : { en_US : en, id_ID : id}})

}