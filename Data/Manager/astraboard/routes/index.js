module.exports = function(server){
  var controller = require(__dirname + "/../controllers")(server)
  
  server.get('/boards', controller.boards)
  server.get('/board/meta', controller.boardMeta)
  server.get('/board/periods', controller.periods)

  server.post('/title/edit', controller.editTitle)
  server.post('/menutitle/edit', controller.editMenuTitle)

  server.get('/period/show', controller.showPeriod)
  server.post('/period/add', controller.addPeriod)
  server.post('/period/remove', controller.removePeriod)
  server.post('/period/edit', controller.editPeriod)

  server.get('/people', controller.people)
  server.get('/person/show', controller.showPerson)
  server.post('/person/add', controller.addPerson)
  server.post('/person/remove', controller.removePerson)
  server.post('/person/edit', controller.editPerson)

  server.post('/image', controller.image)
  server.post('/image/save', controller.imageSave)

  server.post('/people/arrange', controller.arrangePeople)
}