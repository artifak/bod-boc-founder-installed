var _ = require('lodash')

exports.wrap = function(status, data){
  var obj = {
      status : {
        message : 'success'
      },
      data : data
    }

  if(status == 'success'){
    return obj
  }else{
    obj.status.message = 'error'
  }
}